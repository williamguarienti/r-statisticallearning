set.seed(230322)

number_of_repetitions = 1000
nfold = 5

                      
#Generate data
ncol = 100
nrow = 50
beta = rep(0,ncol)
beta[c(1,10,20,50,90)] = rep(5,5)
vars = paste("V",c(1,10,20,50,90), sep = "")

n_right = replicate(number_of_repetitions, 0)         #how many "right" coefficients were found by LASSO
n_total = replicate(number_of_repetitions, 0)         #number of non-zero coefficients found by LASSO
lambdaOtimo = replicate(number_of_repetitions,0)       #optimum lambda estimated by LASSO
coef_matrix = matrix(nrow=1000,ncol=5)                 #coefficients found by LASSO
colnames(coef_matrix) = vars
MSE = matrix(nrow=1000,ncol=1)                        #MSE of coefficients found by LASSO

for (i in 1:number_of_repetitions)
{
  X = matrix(0, ncol = ncol, nrow = nrow)
  for(j in 1:ncol) X[,j] = rnorm(nrow, sd = sqrt(j)) 

  Y = 20 + X%*%beta + rnorm(nrow)
  
  library("glmnet")
  
  validacaoCruzada = cv.glmnet(X, Y, alpha=1, nfold = nfold)
  #plot(validacaoCruzada)
  lambdaOtimo[i] = validacaoCruzada$lambda.min
  ajuste.otimo = glmnet(X, Y, alpha=1, lambda = lambdaOtimo[i])
  cf = coef(ajuste.otimo)
  nm = rownames(cf)[cf[,1] != 0]
  round(cf[nm,],4)
  n_right[i] = sum(vars %in% nm)
  n_total[i] = length(nm) - 1 #do not count the intercept
  if (n_right[i]==n_total[i]){
    coef_matrix[i,] = cf[vars,]
    MSE[i,] = mean(sapply(rep(5,5)-cf[vars,], function(x) x^2))
  }
  print(paste("Progresso: ",i*100/number_of_repetitions," %"))
}

ret = cbind(n_total,n_right,lambdaOtimo,coef_matrix)

